# haproxy-buildpack

This is a Heroku buildpack for haproxy.

You can specify the haproxy version by putting the full link in the VERSION file.

You must have a haproxy.cfg file in your app's root.

This buildpack compiles and generates a haproxy binary in your app's root directory.

It is a WT custom fork of this: https://github.com/SimpleLegal/haproxy-buildpack.git